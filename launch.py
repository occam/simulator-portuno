import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path    = os.path.dirname(__file__)
binary_path     = os.path.join(scripts_path, "local", "sst-5.0", "bin")
binary          = os.path.join(binary_path, "sst")
portuno_path    = os.path.join(scripts_path, "sst-5.0.1", "sst", "elements", "sst_Portuno")
job_path        = os.getcwd()

object = Occam.load()
configuration = object.configuration("General")

# By default, place output in output path
output_path = object.path()

# Look for output objects
outputs = object.outputs("text/tab-separated-values")

if len(outputs) > 0:
  tsv = outputs[0]
  output_path = tsv.volume()

configuration['output'] = output_path
print("Output in %s" % (configuration['output']))

with open(os.path.join(portuno_path, 'configuration.json'), 'w') as outfile:
  json.dump(configuration, outfile)

# This command will generate the graph for the given SST input file.
args = ["/bin/bash",
        os.path.join(scripts_path, "run.sh")]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run SST
Occam.report(command)
