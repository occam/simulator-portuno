import json
import re
import sys
import os

scripts_path = os.path.dirname(__file__)

f = open('sstinfo.txt', 'r')

sstinfo = {}

currentModuleHash = {}
currentHash = {}
for line in f:
  if line.startswith("ELEMENT "):
    moduleName = line.split()[3]
    sstinfo[moduleName] = {}
    currentModuleHash = sstinfo[moduleName]
  if line.startswith("      COMPONENT "):
    componentName = line.split()[3]
    currentModuleHash[componentName] = {}
    currentHash = currentModuleHash[componentName]
    currentHash["parameters"] = {}
  if line.startswith("            PARAMETER"):
    parameterName = line.split()[3]
    default = line.split()[-1][1:-1]
    description = ' '.join(line.split()[4:-1])[1:-1]
    currentHash["parameters"][parameterName] = {}
    if default != "" and default != "REQUIRED":
      currentHash["parameters"][parameterName]["default"] = default
    if description != "":
      currentHash["parameters"][parameterName]["description"] = description
    currentHash["parameters"][parameterName]["type"] = "string"

f.close()
f = open('sst-partition-output.txt', 'r')

schema = {}
currentComponent = {}

exp = re.compile("^\s\s\s\S")
for line in f:
  if exp.match(line):
    componentName = line.split()[0]
    schema[componentName] = {}
    currentComponent = schema[componentName]
  elif line.startswith("      -> type"):
    componentString = ' '.join(line.split()[2:])
    components = componentString.split('.')
    module = components[0]
    components = components[1:]
    moduleInfo = {}
    if module in sstinfo:
      moduleInfo = sstinfo[module]
      for component in components:
        if component in moduleInfo:
          moduleInfo = moduleInfo[component]
        else:
          print("Error: cannot find component %s in module %s" % (component, module))
      for k,v in moduleInfo["parameters"].items():
        currentComponent[k] = v
    else:
      print("Error: cannot find module %s" % (module))

print(json.dumps(schema, indent=2, separators=(',', ': ')))
if not os.path.exists("new_simulator"):
  os.mkdir("new_simulator");
f.close()
f = open('new_simulator/input_schema.json', 'w+')
f.write(json.dumps(schema, indent=2, separators=(',', ': ')))
f.close()

# Copy sim-launch.py
sim_launch_py_path = os.path.join(scripts_path, "sim-launch.py")
os.system('cp %s new_simulator/.' % (sim_launch_py_path))

# Copy occam.py
occam_py_path = os.path.join(scripts_path, "occam.py")
os.system('cp %s new_simulator/.' % (occam_py_path))

# Copy configuration script
simulator_py_path = "simulator.py"
os.system('cp %s new_simulator/.' % (simulator_py_path))
