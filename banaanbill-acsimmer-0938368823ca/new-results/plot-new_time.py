#!/usr/bin/env python

import os
import errno
import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as p
import csv

FIGDIR = 'fig-new'
DATA = 'latest-new'
p.rcParams.update({
	'figure.figsize': (4.8, 3.6),
	'figure.autolayout': True,
	'savefig.format': 'pdf',
	'legend.numpoints': 1,
	'legend.loc': 0,
	'legend.fontsize': 12,
	'legend.handlelength': 1,
	'font.family': 'serif',
	'text.usetex': True
})

MARKER = {
	'gsis.ImplementPcInRbac1': '+',
	'gsis.indirect.ImplementPcInRbac0': 'o',
	'gsis.indirect.ImplementPcInUgo': '^',
	'time': 'o'
}
MEC = {
	'gsis.ImplementPcInRbac1': '#006666',
	'gsis.indirect.ImplementPcInRbac0': '#000066',
	'gsis.indirect.ImplementPcInUgo': '#660000',
	'time': '#000000'
}
SCHEMES = {
	'gsis.ImplementPcInRbac1': 'ImplementPcInRbac1',
	'gsis.indirect.ImplementPcInRbac0': 'ImplementPcInRbac0',
	'gsis.indirect.ImplementPcInUgo': 'ImplementPcInUgo'
}

def read(data):
	data = open(data)
	cols = None
	datas = {}
	reader = csv.reader(data, delimiter='\t')
	for row in reader:
		if cols == None:
			cols = []
			for heading in row:
				cols.append(heading)
				datas[heading] = []
		else:
			i = 0
			for shite in row:
				try:
					datas[cols[i]].append(int(shite))
				except ValueError:
					try:
						datas[cols[i]].append(float(shite))
					except ValueError:
						datas[cols[i]].append(0)
				i+=1
	return datas

def plank(x, y, sc):
	return p.plot(x, y, ls='None', c='black', marker=MARKER[sc], mec=MEC[sc], fillstyle='none', ms=5)

def mainplot(d):
	p.figure()
	p0 = p.plot(d['gsis.ImplementPcInRbac1-Number_Of_Actors'], d['-Time-'], ls='None', c='black', marker=MARKER['time'], mec=MEC['time'], fillstyle='none', ms=5)
	p.xlabel('Number of Actors')
	p.ylabel('Simulation Time (sec)')
	p.savefig(FIGDIR + '/SimulationTime.pdf')
	p.close()

def main():
	try:
		os.makedirs(FIGDIR)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise
	d = read(DATA + '.tsv')
	mainplot(d)
main()
