package ibe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import base.Conversion;
import base.Driver;
import base.Measure;
import base.Params;
import base.Simulation;

import org.json.*;
import java.io.*;
import java.util.*;

public class DriverIbe extends Driver {
    private Params param;

    private static final int NUM_RUNS = 100;
    @Override
    public int getNumRuns() {
        return NUM_RUNS;
    }

    @Override
    public String generatePlotter() {
        if(param == null)
            return null;
        List<Measure> plots = new ArrayList<Measure>();
        try {
            for(String mname : param.measures) {
                plots.add((Measure)Class.forName(mname).newInstance());
            }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        Collections.sort(plots);

        return super.getPlotter(param.implementationClassNames, plots);
    }

    @Override
    public Params generateSimulation() {
        param = new Params();

        param.addBias = 0.7 + (Simulation.rand.nextDouble() * 0.3);
        param.urBias = 0.3 + (Simulation.rand.nextDouble() * 0.4);

        //NewPcStarter starter = new NewPcStarter(numrev, numauth);
        param.starter = new StarterIbe();
        param.implementationClassNames = new String[] {
            "ibe.ImplementRbac0WInPtIbe",
            "ibe.ImplementRbac0WInPubKey",
            "ibe.ImplementRbac0WInSymKey",
            "ibe.ImplementRbac0WInSymKeyDel"
        };

        // X-axis measures
        param.measures.add("ibe.measure.AddBias");
        param.measures.add("ibe.measure.UrBias");

        // 2G measures
        param.measures.add("ibe.measure.AsymAuth");
        param.measures.add("ibe.measure.AsymDecCl");
        param.measures.add("ibe.measure.AsymDecSu");
        param.measures.add("ibe.measure.AsymEncCl");
        param.measures.add("ibe.measure.AsymEncSu");
        param.measures.add("ibe.measure.AsymGenCl");
        param.measures.add("ibe.measure.AsymGenSu");
        param.measures.add("ibe.measure.FileClToRm");
        param.measures.add("ibe.measure.FileClToSt");
        param.measures.add("ibe.measure.FileRmToCl");
        param.measures.add("ibe.measure.FileRmToSt");
        param.measures.add("ibe.measure.FileStToCl");
        param.measures.add("ibe.measure.FileStToRm");
        param.measures.add("ibe.measure.IbeDecCl");
        param.measures.add("ibe.measure.IbeDecSu");
        param.measures.add("ibe.measure.IbeEncCl");
        param.measures.add("ibe.measure.IbeEncSu");
        param.measures.add("ibe.measure.IbeGenSu");
        param.measures.add("ibe.measure.IbsAuth");
        param.measures.add("ibe.measure.KeyClToSt");
        param.measures.add("ibe.measure.KeyRmToCl");
        param.measures.add("ibe.measure.KeyStToCl");
        param.measures.add("ibe.measure.KeyStToSu");
        param.measures.add("ibe.measure.KeySuToSt");
        param.measures.add("ibe.measure.SymDecCl");
        param.measures.add("ibe.measure.SymDecRm");
        param.measures.add("ibe.measure.SymEncCl");
        param.measures.add("ibe.measure.SymEncRm");
        param.measures.add("ibe.measure.SymGenCl");
        param.measures.add("ibe.measure.SymGenRm");
        param.measures.add("ibe.measure.SymGenSu");

        // 3G measures
        param.measures.add("ibe.measure.IbeEncRevU");
        param.measures.add("ibe.measure.IbeEncPerUserRevoked");
        param.measures.add("ibe.measure.IbeEncRevP");
        param.measures.add("ibe.measure.IbeEncPerPermRevoked");
        param.measures.add("ibe.measure.FileRekeyRevU");
        param.measures.add("ibe.measure.FileRekeyPerUserRevoked");
        param.measures.add("ibe.measure.FileRekeyRevP");
        param.measures.add("ibe.measure.FileRekeyPerPermRevoked");

        param.timelimitPerSimulation = 1 * Conversion.Months;
        param.delta = 1 * Conversion.Hours; // hours

        param.workflowName = "ibe.WorkflowNull";

        return param;
    }

	@Override
	public Params generateSimulationFromConfig(String configPath) {
		try {
			String jsonData = readFile(configPath);
			JSONObject jobj = new JSONObject(jsonData);

			param = new Params();

			param.addBias = jobj.getDouble("addbias_addfactor") + (Simulation.rand.nextDouble() * jobj.getDouble("addbias_mulfactor"));
        	param.urBias = jobj.getDouble("urbias_addfactor") + (Simulation.rand.nextDouble() * jobj.getDouble("urbias_mulfactor"));

			String init = jobj.getString("starters");

			param.starter = new StarterIbe(init);

			List<String> implsList = new ArrayList<String>();

			JSONObject implsObj = jobj.getJSONObject("List of implementations");
			
			if(implsObj.getBoolean("rbac0WInPtIbe")) {
				implsList.add("ibe.ImplementRbac0WInPtIbe");
			}
			if(implsObj.getBoolean("rbac0WInPubKey")) {
				implsList.add("ibe.ImplementRbac0WInPubKey");
			}
			if(implsObj.getBoolean("rbac0WInSymKey")) {
				implsList.add("ibe.ImplementRbac0WInSymKey");
			}
			if(implsObj.getBoolean("rbac0WInSymKeyDel")) {
				implsList.add("ibe.ImplementRbac0WInSymKeyDel");
			}

			param.implementationClassNames = implsList.toArray(new String[implsList.size()]);

			JSONObject measuresObj = jobj.getJSONObject("List of measurements");

			// X-axis measures
			if(measuresObj.getBoolean("addBias")) {
				param.measures.add("ibe.measure.AddBias");
			}
			if(measuresObj.getBoolean("urBias")) {
				param.measures.add("ibe.measure.UrBias");
			}

			// 2G measures
			if(measuresObj.getBoolean("asymAuth")) {
				param.measures.add("ibe.measure.AsymAuth");
			}
			if(measuresObj.getBoolean("asymDecCl")) {
				param.measures.add("ibe.measure.AsymDecCl");
			}
			if(measuresObj.getBoolean("asymDecSu")) {
				param.measures.add("ibe.measure.AsymDecSu");
			}
			if(measuresObj.getBoolean("asymEncCl")) {
				param.measures.add("ibe.measure.AsymEncCl");
			}
			if(measuresObj.getBoolean("asymEncSu")) {
				param.measures.add("ibe.measure.AsymEncSu");
			}
			if(measuresObj.getBoolean("asymGenCl")) {
				param.measures.add("ibe.measure.AsymGenCl");
			}
			if(measuresObj.getBoolean("asymGenSu")) {
				param.measures.add("ibe.measure.AsymGenSu");
			}

			if(measuresObj.getBoolean("fileClToRm")) {
				param.measures.add("ibe.measure.FileClToRm");
			}
			if(measuresObj.getBoolean("fileClToSt")) {
				param.measures.add("ibe.measure.FileClToSt");
			}
			if(measuresObj.getBoolean("fileRmToCl")) {
				param.measures.add("ibe.measure.FileRmToCl");
			}
			if(measuresObj.getBoolean("fileRmToSt")) {
				param.measures.add("ibe.measure.FileRmToSt");
			}
			if(measuresObj.getBoolean("fileStToCl")) {
				param.measures.add("ibe.measure.FileStToCl");
			}
			if(measuresObj.getBoolean("fileStToRm")) {
				param.measures.add("ibe.measure.FileStToRm");
			}

			if(measuresObj.getBoolean("ibeDecCl")) {
				param.measures.add("ibe.measure.IbeDecCl");
			}
			if(measuresObj.getBoolean("ibeDecSu")) {
				param.measures.add("ibe.measure.IbeDecSu");
			}
			if(measuresObj.getBoolean("ibeEncCl")) {
				param.measures.add("ibe.measure.IbeEncCl");
			}
			if(measuresObj.getBoolean("ibeEncSu")) {
				param.measures.add("ibe.measure.IbeEncSu");
			}
			if(measuresObj.getBoolean("ibeGenSu")) {
				param.measures.add("ibe.measure.IbeGenSu");
			}
			if(measuresObj.getBoolean("ibsAuth")) {
				param.measures.add("ibe.measure.IbsAuth");
			}

			if(measuresObj.getBoolean("keyClToSt")) {
				param.measures.add("ibe.measure.KeyClToSt");
			}
			if(measuresObj.getBoolean("keyRmToCl")) {
				param.measures.add("ibe.measure.KeyRmToCl");
			}
			if(measuresObj.getBoolean("keyStToCl")) {
				param.measures.add("ibe.measure.KeyStToCl");
			}
			if(measuresObj.getBoolean("keyStToSu")) {
				param.measures.add("ibe.measure.KeyStToSu");
			}
			if(measuresObj.getBoolean("keySuToSt")) {
				param.measures.add("ibe.measure.KeySuToSt");
			}

			if(measuresObj.getBoolean("symDecCl")) {
				param.measures.add("ibe.measure.SymDecCl");
			}
			if(measuresObj.getBoolean("symDecRm")) {
				param.measures.add("ibe.measure.SymDecRm");
			}
			if(measuresObj.getBoolean("symEncCl")) {
				param.measures.add("ibe.measure.SymEncCl");
			}
			if(measuresObj.getBoolean("symEncRm")) {
				param.measures.add("ibe.measure.SymEncRm");
			}
			if(measuresObj.getBoolean("symGenCl")) {
				param.measures.add("ibe.measure.SymGenCl");
			}
			if(measuresObj.getBoolean("symGenRm")) {
				param.measures.add("ibe.measure.SymGenRm");
			}
			if(measuresObj.getBoolean("symGenSu")) {
				param.measures.add("ibe.measure.SymGenSu");
			}

			// 3G measures
			if(measuresObj.getBoolean("ibeEncRevU")) {
				param.measures.add("ibe.measure.IbeEncRevU");
			}
			if(measuresObj.getBoolean("ibeEncPerUserRevoked")) {
				param.measures.add("ibe.measure.IbeEncPerUserRevoked");
			}
			if(measuresObj.getBoolean("ibeEncRevP")) {
				param.measures.add("ibe.measure.IbeEncRevP");
			}
			if(measuresObj.getBoolean("ibeEncPerPermRevoked")) {
				param.measures.add("ibe.measure.IbeEncPerPermRevoked");
			}
			if(measuresObj.getBoolean("fileRekeyRevU")) {
				param.measures.add("ibe.measure.FileRekeyRevU");
			}
			if(measuresObj.getBoolean("fileRekeyPerUserRevoked")) {
				param.measures.add("ibe.measure.FileRekeyPerUserRevoked");
			}
			if(measuresObj.getBoolean("fileRekeyPerPermRevoked")) {
				param.measures.add("ibe.measure.FileRekeyPerPermRevoked");
			}

			param.timelimitPerSimulation = 1 * Conversion.Months;
			param.delta = 1 * Conversion.Hours; // hours

			param.workflowName = "ibe.WorkflowNull";
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return param;
	}

	public static String readFile(String filename) {
	    String result = "";
	    try {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		while (line != null) {
		    sb.append(line);
		    line = br.readLine();
		}
		result = sb.toString();
	    } catch(Exception e) {
		e.printStackTrace();
	    }
	    return result;
	}

}

