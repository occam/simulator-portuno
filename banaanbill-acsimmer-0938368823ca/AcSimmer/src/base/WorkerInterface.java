package base;
import java.io.*;

import java.util.Date;

public class WorkerInterface {
	public static final int SERVER_PORT = 20000;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
        String outputFileName;
        try {
            String workerId = args[0];
            String iterationIndex = args[1];
            int runIndex = (java.lang.Integer.parseInt(iterationIndex) - 1) * 4 + java.lang.Integer.parseInt(workerId);
            String path = args[2];
            String paramFile = path + "/param_" + workerId + "_" + iterationIndex + ".txt";
            String outputFile = path + "/output_" + workerId + "_" + iterationIndex + ".txt";
            
            Params param = null;
            
            ObjectInputStream objectinputstream = null;
            FileInputStream streamIn = null;
            
            try {
                streamIn = new FileInputStream(paramFile);
                objectinputstream = new ObjectInputStream(streamIn);
                param = (Params) objectinputstream.readObject();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(objectinputstream != null){
                    streamIn.close();
                    objectinputstream.close();
                } 
            }
            
            if (param != null) {
                Simulation sim = new Simulation(param);
                sim.build();
						
                long preTime = new Date().getTime();

                String res = "" + runIndex + sim.sep;
						
                try {
                    sim.run();
                } catch(Exception e) {
                    e.printStackTrace();
                }
                
                double seconds = (new Date().getTime() - preTime) / 1000.0;
                
                res += sim.results(false);
                res += sim.sep + seconds;
                
                PrintWriter out = null;
                try {
                    out = new PrintWriter(outputFile);
                    out.println(res);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(out != null){
                        out.close();
                    }
                }
            }
        } catch(Exception e) {
        }
    }
}

