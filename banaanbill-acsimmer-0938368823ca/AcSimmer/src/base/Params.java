package base;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * This class is used to store inital paramters.
 *
 * There might be a better way to code this up, but so far I don't see a reason to change it.
 * @author yechen
 *
 */
public class Params implements Serializable {

	private static final long serialVersionUID = -4057058215338139666L;

	/**
	 * What measurements are we concerned about?
	 */
	public HashSet<String> measures = new HashSet<String>();

	public String[] implementationClassNames = {}; //These are implementations we will instantiate in Sim.

	public SimulationStarter starter; //How do we build initial state and actor machine?

	public String workflowName = ""; //The workflow class name to be instantiated.

	// General params
	public double delta;
	public double timelimitPerSimulation;
	public System system = null;

	// IBE params
	public double addBias;
	public double urBias;

	// Actor-machine-specific params
	public double coi_freqm;
	public double post_freqm;
	public double churn_factor;
	public double del_factor;
	public double create_post_factor;

	// newpc params
	public int numAuthors;
	public int numReviewers;
	public double rotateRate;
	public double conflictRate;
	public double discussRate;
	// used for keeping track of things...
	public boolean reviewGroupsAdded = false;
	public boolean summariesAdded = false;
	public int currentPaper = 1;
	public int nextReview = 1;
	public int nextMessage = 1;
	public LinkedList<String> conflicted = new LinkedList<String>();

	// These are used for testing
	public double freqA = 1.0/60;
	public double freqB = 1.0;

	/**
	 * Current run's index, useful for printing in client-server environment.
	 */
	public int runIndex;

	public Params(Params p) {
		measures = new HashSet<String>(p.measures);

		implementationClassNames = p.implementationClassNames;
		starter = p.starter;
		workflowName = p.workflowName;
		delta = p.delta;
		timelimitPerSimulation = p.timelimitPerSimulation;
		system = p.system;
		coi_freqm = p.coi_freqm;
		post_freqm = p.post_freqm;
		churn_factor = p.churn_factor;
		del_factor = p.del_factor;
		create_post_factor = p.create_post_factor;
		numAuthors = p.numAuthors;
		numReviewers = p.numReviewers;
		rotateRate = p.rotateRate;
		conflictRate = p.conflictRate;
		discussRate = p.discussRate;
		reviewGroupsAdded = p.reviewGroupsAdded;
		summariesAdded = p.summariesAdded;
		currentPaper = p.currentPaper;
		nextReview = p.nextReview;
		nextMessage = p.nextMessage;

		conflicted = new LinkedList<String>(p.conflicted);

		freqA = p.freqA;
		freqB = p.freqB;
		runIndex = p.runIndex;
	}

	public Params() {}
}
