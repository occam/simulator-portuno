package base;
import java.io.*;

public class ManagerInterface {
	public static final String OUTPUT_FILE = "latest.tsv";

	public static void main(String[] args) {
		try {
			String driverClass = args[0];
			String config = args[1];
			String path = args[2];
			int count = java.lang.Integer.parseInt(args[3]);

			Class driverC = Class.forName(driverClass);
			Driver driver = (Driver) driverC.newInstance();

			for (int i = 1; i <= count; i++) {
				String PARAM_FILE = "p_" + i + ".txt";

				Params param = driver.generateSimulationFromConfig(config);
				param.runIndex = i;

				if (i == 1) {
					String ret = "";

					Simulation sim = new Simulation(param);
					sim.build();

					ret += "-Run-" + sim.sep;
					ret += sim.headerStr(false);
					ret += sim.sep + "-Time-" + "\n";

					File file = new File(path + "/"+ OUTPUT_FILE);
					FileWriter writer = new FileWriter(file);
					writer.append(ret);
					writer.close();
				}

				ObjectOutputStream oos = null;
				FileOutputStream fout = null;
				try{
					fout = new FileOutputStream(path + "/"+ PARAM_FILE, true);
					oos = new ObjectOutputStream(fout);
					oos.writeObject(param);
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
				if(oos  != null){
					oos.close();
				}
			}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
