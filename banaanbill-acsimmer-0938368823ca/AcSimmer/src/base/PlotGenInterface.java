package base;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class PlotGenInterface {

	public static void main(String[] args) {
		try {
            String className = args[0];
            String path = args[1];
            String filename = args[2];
            
			Class driverC = Class.forName(className);
			Driver driver = (Driver) driverC.newInstance();
			driver.generateSimulation();
			String res = driver.generatePlotter();
            
            File file = new File(path + "/" + filename);
            FileWriter writer = new FileWriter(file);
            writer.append(res);
            writer.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
