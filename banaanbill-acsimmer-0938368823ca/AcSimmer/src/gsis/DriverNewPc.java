package gsis;

import java.util.ArrayList;
import java.util.Collections;

import base.Conversion;
import base.Driver;
import base.Measure;
import base.Params;
import base.Simulation;

import org.json.*;
import java.io.*;
import java.util.*;

public class DriverNewPc extends Driver {
	private Params param;

	@Override
	public int getNumRuns() {
		return super.getNumRuns();
	}

	@Override
	public String generatePlotter() {
		if(param == null)
			return null;
		ArrayList<Measure> plots = new ArrayList<Measure>();
		try {
			for(String mname : param.measures) {
				Class measureClass = Class.forName(mname);
				Measure measure = (Measure) measureClass.newInstance();
				plots.add(measure);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		Collections.sort(plots);

		String ret = super.getPlotter(param.implementationClassNames, plots);

		return ret;
	}

	@Override
	public Params generateSimulation() {
		param = new Params();
		int numrev = Simulation.rand.nextInt(55) + 20;
		int numauth = numrev * 3;

		NewPcStarter starter = new NewPcStarter(numrev, numauth);
		param.starter = starter;
		String[] impls = {
			"gsis.ImplementPcInRbac1",
			"gsis.indirect.ImplementPcInRbac0",
			"gsis.indirect.ImplementPcInUgo"
		};
		param.implementationClassNames = impls;

		param.numAuthors = numauth;
		param.numReviewers = numrev;

		param.rotateRate = 1.0 / (6 * Conversion.Hours);
		param.conflictRate = 0.03;
		param.discussRate = 1.0 / (16 * Conversion.Hours);

		// Counts
		param.measures.add("measures.NumberOfActors");
		param.measures.add("measures.NumberOfAuthors");
		param.measures.add("measures.NumberOfReviewers");
		param.measures.add("measures.NumberOfObjects");
		param.measures.add("measures.StutterAvg");
		param.measures.add("measures.StutteringProportion");
		// param.measures.add("measures.rotateRate");
		// param.measures.add("measures.conflictRate");

		// Scheme IO
		// param.measures.add("measures.SchemeIOAddGAvg");
		// param.measures.add("measures.SchemeIOAddOAvg");
		// param.measures.add("measures.SchemeIOSJoinAvg");
		// param.measures.add("measures.SchemeIOLJoinAvg");
		// param.measures.add("measures.SchemeIOSLeaveAvg");
		// param.measures.add("measures.SchemeIOLLeaveAvg");
		// param.measures.add("measures.SchemeIOLAddAvg");
		param.measures.add("measures.SchemeIO");

		// Workload IO
		// param.measures.add("measures.WorkloadIOAddGAvg");
		// param.measures.add("measures.WorkloadIOAddOAvg");
		// param.measures.add("measures.WorkloadIOSJoinAvg");
		// param.measures.add("measures.WorkloadIOLJoinAvg");
		// param.measures.add("measures.WorkloadIOSLeaveAvg");
		// param.measures.add("measures.WorkloadIOLLeaveAvg");
		// param.measures.add("measures.WorkloadIOLAddAvg");
		param.measures.add("measures.WorkloadIO");

		param.measures.add("measures.MaxStateSizeMeasure");
		param.measures.add("measures.MaxWStateSizeMeasure");
		param.measures.add("measures.RBAC_MaxRoles");
		param.measures.add("measures.SumStutterMeasure");
		param.measures.add("measures.WorkloadStateChangeProportion");
		param.measures.add("measures.ACStateChangeProportion");
		param.measures.add("measures.PermissionsPerRoleMeasure");

		// none of these apply here...
		//param.create_post_factor = 2;
		//param.churn_factor = 1;
		//param.del_factor = 1;
		//param.post_freqm = 1;

		param.timelimitPerSimulation = PcPhases.totalDur();
		param.delta = 1 * Conversion.Hours; // hours

		param.workflowName = "gsis.WorkflowFlat";

		return param;
	}

	@Override
	public Params generateSimulationFromConfig(String configPath) {
		try {
			String jsonData = readFile(configPath);
			JSONObject jobj = new JSONObject(jsonData);

			param = new Params();
			int numrev = Simulation.rand.nextInt(jobj.getInt("max_reviewer") - jobj.getInt("min_reviewer")) + jobj.getInt("min_reviewer");
			int numauth = numrev * jobj.getInt("author_per_reviewer");

			NewPcStarter starter = new NewPcStarter(numrev, numauth);
			param.starter = starter;

			List<String> implsList = new ArrayList<String>();

			JSONObject implsObj = jobj.getJSONObject("List of implementations");
			
			if(implsObj.getBoolean("rbac1")) {
				implsList.add("gsis.ImplementPcInRbac1");
			}
			if(implsObj.getBoolean("rbac0")) {
				implsList.add("gsis.indirect.ImplementPcInRbac0");
			}
			if(implsObj.getBoolean("ugo")) {
				implsList.add("gsis.indirect.ImplementPcInUgo");
			}

			param.implementationClassNames = implsList.toArray(new String[implsList.size()]);

			param.numAuthors = numauth;
			param.numReviewers = numrev;

			param.rotateRate = 1.0 / (6 * Conversion.Hours);
			param.conflictRate = 0.03;
			param.discussRate = 1.0 / (16 * Conversion.Hours);

			JSONObject measuresObj = jobj.getJSONObject("List of measurements");

			// Counts
			if(measuresObj.getBoolean("number_of_actors")) {
				param.measures.add("measures.NumberOfActors");
			}
			if(measuresObj.getBoolean("number_of_authors")) {
				param.measures.add("measures.NumberOfAuthors");
			}
			if(measuresObj.getBoolean("number_of_reviewers")) {
				param.measures.add("measures.NumberOfReviewers");
			}
			if(measuresObj.getBoolean("number_of_objects")) {
				param.measures.add("measures.NumberOfObjects");
			}
			if(measuresObj.getBoolean("stutter_avg")) {
				param.measures.add("measures.StutterAvg");
			}
			if(measuresObj.getBoolean("stuttering_proportion")) {
				param.measures.add("measures.StutteringProportion");
			}

			// Scheme IO
			if(measuresObj.getBoolean("scheme_io")) {
				param.measures.add("measures.SchemeIO");
			}

			// Workload IO
			if(measuresObj.getBoolean("workload_io")) {
				param.measures.add("measures.WorkloadIO");
			}

			if(measuresObj.getBoolean("max_state_size")) {
				param.measures.add("measures.MaxStateSizeMeasure");
			}
			if(measuresObj.getBoolean("max_wstate_size")) {
				param.measures.add("measures.MaxWStateSizeMeasure");
			}
			if(measuresObj.getBoolean("rbac_max_roles")) {
				param.measures.add("measures.RBAC_MaxRoles");
			}
			if(measuresObj.getBoolean("sum_stutter")) {
				param.measures.add("measures.SumStutterMeasure");
			}
			if(measuresObj.getBoolean("workload_state_change_proportion")) {
				param.measures.add("measures.WorkloadStateChangeProportion");
			}
			if(measuresObj.getBoolean("ac_state_change_proportion")) {
				param.measures.add("measures.ACStateChangeProportion");
			}
			if(measuresObj.getBoolean("permissions_per_role")) {
				param.measures.add("measures.PermissionsPerRoleMeasure");
			}

			param.timelimitPerSimulation = PcPhases.totalDur();
			param.delta = 1 * Conversion.Hours; // hours

			param.workflowName = "gsis.WorkflowFlat";
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return param;
	}

	public static String readFile(String filename) {
	    String result = "";
	    try {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		while (line != null) {
		    sb.append(line);
		    line = br.readLine();
		}
		result = sb.toString();
	    } catch(Exception e) {
		e.printStackTrace();
	    }
	    return result;
	}

}

