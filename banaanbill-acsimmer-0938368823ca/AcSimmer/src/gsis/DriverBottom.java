package gsis;

import java.util.ArrayList;
import java.util.Collections;

import base.Driver;
import base.Measure;
import base.Params;
import base.Simulation;

import org.json.*;
import java.io.*;
import java.util.*;

public class DriverBottom extends Driver {
	static final double CHURN_MAX = 4.0;
	static final double POSTF_MAX = 3.0;
	static final double DEL_MAX = 1.0;
	private Params param;

	@Override
	public int getNumRuns() {
		return super.getNumRuns();
	}

	@Override
	public String generatePlotter() {
		if(param == null)
			return null;
		ArrayList<Measure> plots = new ArrayList<Measure>();
		try {
			for(String mname : param.measures) {
				Class measureClass = Class.forName(mname);
				Measure measure = (Measure) measureClass.newInstance();
				plots.add(measure);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		Collections.sort(plots);

		String ret = super.getPlotter(param.implementationClassNames, plots);

		return ret;
	}

	@Override
	public Params generateSimulation() {
		param = new Params();
		int managers, posters, readers;
		posters = Simulation.rand.nextInt(40) + 25;
		readers = 0;
		managers = (int)(posters * Simulation.rand.nextDouble()/5.0) + 5;

		InitBottomClean starter = new InitBottomClean(managers, posters, readers);
		param.starter = starter;
		String[] impls = {
			"gsis.ImplementBottomInRbac1", "gsis.indirect.ImplementBottomInRbac0", "gsis.indirect.ImplementBottomInUgo"
		};
		param.implementationClassNames = impls;

		// Counts
		param.measures.add("measures.NumberOfActors");
		param.measures.add("measures.NumberOfPosters");
		param.measures.add("measures.NumberOfManagers");
		param.measures.add("measures.NumberOfObjects");

		// Scheme IO
		param.measures.add("measures.SchemeIOAddGAvg");
		param.measures.add("measures.SchemeIOAddOAvg");
		param.measures.add("measures.SchemeIOLJoinAvg");
		param.measures.add("measures.SchemeIOLLeaveAvg");
		param.measures.add("measures.SchemeIOLAddAvg");
		param.measures.add("measures.SchemeIOLRemoveAvg");
		param.measures.add("measures.SchemeIO");

		// Workload IO
		param.measures.add("measures.WorkloadIOAddGAvg");
		param.measures.add("measures.WorkloadIOAddOAvg");
		param.measures.add("measures.WorkloadIOLJoinAvg");
		param.measures.add("measures.WorkloadIOLLeaveAvg");
		param.measures.add("measures.WorkloadIOLAddAvg");
		param.measures.add("measures.WorkloadIOLRemoveAvg");
		param.measures.add("measures.WorkloadIO");

		param.measures.add("measures.MaxStateSizeMeasure");
		param.measures.add("measures.MaxWStateSizeMeasure");
		param.measures.add("measures.RBAC_MaxRoles");
		param.measures.add("measures.SumStutterMeasure");
		param.measures.add("measures.WorkloadStateChangeProportion");
		param.measures.add("measures.ACStateChangeProportion");
		param.measures.add("measures.PermissionsPerRoleMeasure");

		//If a user created a new group, the frequency of the user creating a single
		//post is multiplied by...
		param.create_post_factor = 1;
		param.churn_factor = 1;
		param.del_factor = 1;
		param.post_freqm = 1;
		param.timelimitPerSimulation=24 * 3;
		param.delta = 1;
		param.workflowName = "gsis.WorkflowTrivial";

		return param;
	}

	@Override
	public Params generateSimulationFromConfig(String configPath) {
		try {
			String jsonData = readFile(configPath);
			JSONObject jobj = new JSONObject(jsonData);

			param = new Params();

			int managers, posters, readers;
			posters = Simulation.rand.nextInt(jobj.getInt("max_posters") - jobj.getInt("min_posters")) + jobj.getInt("min_posters");
			readers = 0;
			managers = (int)(posters * Simulation.rand.nextDouble()/5.0) + 5;

			InitBottomClean starter = new InitBottomClean(managers, posters, readers);
			param.starter = starter;

			List<String> implsList = new ArrayList<String>();

			JSONObject implsObj = jobj.getJSONObject("List of implementations");

			if(implsObj.getBoolean("rbac1")) {
				implsList.add("gsis.ImplementBottomInRbac1");
			}
			if(implsObj.getBoolean("rbac0")) {
				implsList.add("gsis.indirect.ImplementBottomInRbac0");
			}
			if(implsObj.getBoolean("ugo")) {
				implsList.add("gsis.indirect.ImplementBottomInUgo");
			}

			param.implementationClassNames = implsList.toArray(new String[implsList.size()]);

			JSONObject measuresObj = jobj.getJSONObject("List of measurements");

			// Counts
			if(measuresObj.getBoolean("number_of_actors")) {
				param.measures.add("measures.NumberOfActors");
			}
			if(measuresObj.getBoolean("number_of_posters")) {
				param.measures.add("measures.NumberOfPosters");
			}
			if(measuresObj.getBoolean("number_of_managers")) {
				param.measures.add("measures.NumberOfManagers");
			}
			if(measuresObj.getBoolean("number_of_objects")) {
				param.measures.add("measures.NumberOfObjects");
			}

			// Scheme IO
			if(measuresObj.getBoolean("scheme_io_add_g_avg")) {
				param.measures.add("measures.SchemeIOAddGAvg");
			}
			if(measuresObj.getBoolean("scheme_io_add_o_avg")) {
				param.measures.add("measures.SchemeIOAddOAvg");
			}
			if(measuresObj.getBoolean("scheme_io_l_join_avg")) {
				param.measures.add("measures.SchemeIOLJoinAvg");
			}
			if(measuresObj.getBoolean("scheme_io_l_leave_avg")) {
				param.measures.add("measures.SchemeIOLLeaveAvg");
			}
			if(measuresObj.getBoolean("scheme_io_l_add_avg")) {
				param.measures.add("measures.SchemeIOLAddAvg");
			}
			if(measuresObj.getBoolean("scheme_io_l_remove_avg")) {
				param.measures.add("measures.SchemeIOLRemoveAvg");
			}
			if(measuresObj.getBoolean("scheme_io")) {
				param.measures.add("measures.SchemeIO");
			}

			// Workload IO
			if(measuresObj.getBoolean("workload_io_add_g_avg")) {
				param.measures.add("measures.WorkloadIOAddGAvg");
			}
			if(measuresObj.getBoolean("workload_io_add_o_avg")) {
				param.measures.add("measures.WorkloadIOAddOAvg");
			}
			if(measuresObj.getBoolean("workload_io_l_join_avg")) {
				param.measures.add("measures.WorkloadIOLJoinAvg");
			}
			if(measuresObj.getBoolean("workload_io_l_leave_avg")) {
				param.measures.add("measures.WorkloadIOLLeaveAvg");
			}
			if(measuresObj.getBoolean("workload_io_l_add_avg")) {
				param.measures.add("measures.WorkloadIOLAddAvg");
			}
			if(measuresObj.getBoolean("workload_io_l_remove_avg")) {
				param.measures.add("measures.WorkloadIOLRemoveAvg");
			}
			if(measuresObj.getBoolean("workload_io")) {
				param.measures.add("measures.WorkloadIO");
			}

			if(measuresObj.getBoolean("max_state_size")) {
				param.measures.add("measures.MaxStateSizeMeasure");
			}
			if(measuresObj.getBoolean("max_wstate_size")) {
				param.measures.add("measures.MaxWStateSizeMeasure");
			}
			if(measuresObj.getBoolean("rbac_max_roles")) {
				param.measures.add("measures.RBAC_MaxRoles");
			}
			if(measuresObj.getBoolean("sum_stutter")) {
				param.measures.add("measures.SumStutterMeasure");
			}
			if(measuresObj.getBoolean("workload_state_change_proportion")) {
				param.measures.add("measures.WorkloadStateChangeProportion");
			}
			if(measuresObj.getBoolean("ac_state_change_proportion")) {
				param.measures.add("measures.ACStateChangeProportion");
			}
			if(measuresObj.getBoolean("permissions_per_role")) {
				param.measures.add("measures.PermissionsPerRoleMeasure");
			}

			//If a user created a new group, the frequency of the user creating a single
			//post is multiplied by...
			param.create_post_factor = 1;
			param.churn_factor = 1;
			param.del_factor = 1;
			param.post_freqm = 1;
			param.timelimitPerSimulation=24 * 3;
			param.delta = 1;
			param.workflowName = "gsis.WorkflowTrivial";
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return param;
	}

	public static String readFile(String filename) {
	    String result = "";
	    try {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		while (line != null) {
		    sb.append(line);
		    line = br.readLine();
		}
		result = sb.toString();
	    } catch(Exception e) {
		e.printStackTrace();
	    }
	    return result;
	}

}
