ROOT=$(readlink -f $(dirname $0))

cd ${ROOT}

export PYENV_ROOT=`pwd`/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"

pyenv local 2.7.6

export CPATH=$CPATH:`python-config --prefix`/include
export LIBRARY_PATH=$LIBRARY_PATH:`python-config --prefix`/lib

# Determine install path for OpenMPI
export MPIHOME=`pwd`/local/packages/OpenMPI-1.8

# Update PATH
export PATH=$MPIHOME/bin:$PATH
export MPICC=mpicc
export MPICXX=mpicxx

# Determine install path for Boost
export BOOST_HOME=`pwd`/local/packages/boost-1.56

# Determine install path for SST
export SST_HOME=`pwd`/local/sst-5.0
export PATH=${SST_HOME}/bin:$PATH

# We need to run within this directory, unfortunately
cd ${ROOT}/sst-5.0.1/sst/elements/sst_Portuno
../../../../local/sst-5.0/bin/sst --lib-path . sst_Portuno.py
rm Logging*
