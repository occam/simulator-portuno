#ifndef _SST_PORTUNOEVENT_H
#define _SST_PORTUNOEVENT_H

#include <fstream>

namespace SST {
namespace Portuno {

class PortunoEvent : public SST::Event 
{
public:
    typedef std::istream_iterator<char> istream_iterator;
    typedef std::vector<char> dataVec;
    PortunoEvent() : SST::Event() { }
    int runId;
    int waitFlag;
    dataVec payload;

    void populatePayload(std::string fileName) {
	std::ifstream file;
	file.exceptions(
	    std::ifstream::badbit
	  | std::ifstream::failbit
	  | std::ifstream::eofbit);

	file.open(fileName.c_str(), std::ifstream::in | std::ifstream::binary);
	file.seekg(0, std::ios::end);
	std::streampos length(file.tellg());
	if (length) {
	    file.seekg(0, std::ios::beg);
	    payload.resize(static_cast<std::size_t>(length));
	    file.read(&payload.front(), static_cast<std::size_t>(length));
	}
    }

    void populateOutputPayload(std::string outputPath, int workerId, int count) {
	for (int i = 1; i <= count; i++) {
		sleep (2);
		std::string fileName = outputPath + "/output_" + std::to_string(workerId) + "_" + std::to_string(i) + ".txt";
		std::ifstream file(fileName);

        	file >> std::noskipws;
		std::copy(istream_iterator(file), istream_iterator(),
			  std::back_inserter(payload));
	}
    }

private:
    friend class boost::serialization::access;
    template<class Archive>
    void
    serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Event);
	ar & BOOST_SERIALIZATION_NVP(runId);
	ar & BOOST_SERIALIZATION_NVP(waitFlag);
        ar & BOOST_SERIALIZATION_NVP(payload);
    }
};

} // namespace Portuno
} // namespace SST

#endif /* _SIMPLECOMPONENTEVENT_H */
