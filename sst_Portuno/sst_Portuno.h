#ifndef _SST_PORTUNO_H
#define _SST_PORTUNO_H

#include <sst/core/component.h>
#include <sst/core/link.h>

namespace SST {
namespace Portuno {

class sst_Portuno : public SST::Component 
{
public:
    sst_Portuno(SST::ComponentId_t id, SST::Params& params);
    ~sst_Portuno();
    
    //void init(unsigned int phase);
    void setup();
    void finish();

    bool fileExists(std::string fileName);
    void writePayloadToFile(std::vector<char> payload, std::string fileName);

private:
    sst_Portuno();  // for serialization only
    sst_Portuno(const sst_Portuno&); // do not implement
    void operator=(const sst_Portuno&); // do not implement
    
    virtual bool clockTic(SST::Cycle_t);
    
    int my_id;
    int count_to;
    int latency;

    std::string driver;
    std::string configPath;
    std::string binaryPath;
    std::string outputPath;

    int doneCount = 0;

    int waitFlag = 0;
    int runId = 0;

    double start, end;

    SST::Link* N;
    SST::Link* S;
    SST::Link* E;
    SST::Link* W;
    
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Component);
        ar & BOOST_SERIALIZATION_NVP(my_id);
        ar & BOOST_SERIALIZATION_NVP(count_to);
        ar & BOOST_SERIALIZATION_NVP(latency);
        ar & BOOST_SERIALIZATION_NVP(driver);
        ar & BOOST_SERIALIZATION_NVP(configPath);
        ar & BOOST_SERIALIZATION_NVP(binaryPath);
        ar & BOOST_SERIALIZATION_NVP(outputPath);
        ar & BOOST_SERIALIZATION_NVP(doneCount);
        ar & BOOST_SERIALIZATION_NVP(waitFlag);
        ar & BOOST_SERIALIZATION_NVP(runId);
        ar & BOOST_SERIALIZATION_NVP(start);
        ar & BOOST_SERIALIZATION_NVP(end);
        ar & BOOST_SERIALIZATION_NVP(N);
        ar & BOOST_SERIALIZATION_NVP(S);
        ar & BOOST_SERIALIZATION_NVP(E);
        ar & BOOST_SERIALIZATION_NVP(W);
    }
    
    template<class Archive>
    void load(Archive & ar, const unsigned int version) 
    {
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Component);
        ar & BOOST_SERIALIZATION_NVP(my_id);
        ar & BOOST_SERIALIZATION_NVP(count_to);
        ar & BOOST_SERIALIZATION_NVP(latency);
        ar & BOOST_SERIALIZATION_NVP(driver);
        ar & BOOST_SERIALIZATION_NVP(configPath);
        ar & BOOST_SERIALIZATION_NVP(binaryPath);
        ar & BOOST_SERIALIZATION_NVP(outputPath);
        ar & BOOST_SERIALIZATION_NVP(doneCount);
        ar & BOOST_SERIALIZATION_NVP(waitFlag);
        ar & BOOST_SERIALIZATION_NVP(runId);
        ar & BOOST_SERIALIZATION_NVP(start);
        ar & BOOST_SERIALIZATION_NVP(end);
        ar & BOOST_SERIALIZATION_NVP(N);
        ar & BOOST_SERIALIZATION_NVP(S);
        ar & BOOST_SERIALIZATION_NVP(E);
        ar & BOOST_SERIALIZATION_NVP(W);
    }
      
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} // namespace SimpleComponent
} // namespace SST

#endif /* _SIMPLECOMPONENT_H */
