#include "sst_config.h"
#include "sst/core/element.h"
#include "sst_Portuno.h"
#include "sst_PortunoEvent.h"

#include "sst/core/event.h"
#include "sst/core/debug.h"

#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>

using namespace SST;
using namespace SST::Portuno;

sst_Portuno::sst_Portuno(ComponentId_t id, Params& params) :
  Component(id)
{
    bool found;
    
    // get parameters
    Output &out = Simulation::getSimulation()->getSimulationOutput();

    my_id = params.find_integer("id", -1, found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find node id\n");
    }

    count_to = params.find_integer("count_to", 0, found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find count_to\n");
    }
        
    latency = params.find_integer("latency", 0, found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find latency\n");
    }

    driver = params.find_string("driver", "", found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find driver\n");
    }

    configPath = params.find_string("configPath", "", found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find configPath\n");
    }

    binaryPath = params.find_string("binaryPath", "", found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find binaryPath\n");
    }

    outputPath = params.find_string("outputPath", "", found);
    if (!found) {
        out.fatal(CALL_INFO, -1,"couldn't find outputPath\n");
    }
    
    // tell the simulator not to end without us
    registerAsPrimaryComponent();
    primaryComponentDoNotEndSim();
    
    // configure out links
    N = configureLink("Nlink");
    S = configureLink("Slink");
    E = configureLink("Elink");
    W = configureLink("Wlink");

    registerClock("1Hz", new Clock::Handler<sst_Portuno>(this, 
                  &sst_Portuno::clockTic));
}

sst_Portuno::~sst_Portuno() 
{
	
}

sst_Portuno::sst_Portuno() : Component(-1)
{
    // for serialization only
}

void sst_Portuno::finish() {
	switch (my_id) {
		case 4:
			printf("Portuno Worker 4 Finished\n");
			break; 
		case 3: 
			printf("Portuno Worker 3 Finished\n");
			break;   
		case 2:
			printf("Portuno Worker 2 Finished\n");
			break;  
		case 1:
			printf("Portuno Worker 1 Finished\n");
			break;  
		case 0:
			printf("Portuno Manager Finished\n");
			break; 
		default: 
			break;   
	} 
}

void sst_Portuno::setup()
{
	if (my_id == 0) {
		struct timeval startTV;
		gettimeofday(&startTV, NULL);
		start = startTV.tv_sec + startTV.tv_usec / 1e6;		

		std::string managerInterfaceCommand = "java -cp " + binaryPath + "/java-json.jar:" + binaryPath + " base.ManagerInterface " + driver + " " + configPath + " " + outputPath + " " + std::to_string(count_to * 4);
		const char *cmd = managerInterfaceCommand.c_str();
		int ret = system(cmd);

		for (int i = 1; i <= 4; i++) {
			std::string paramFile = outputPath + "/p_" + std::to_string(i)  + ".txt";
			PortunoEvent *event = new PortunoEvent();
			event -> runId = 1;
			event -> waitFlag = 0;
			event -> populatePayload(paramFile);
			switch(i) {
				case 4:
					N->send(latency,event);
					printf("Manager sending work to Worker 4 at iteration %d\n\n", event -> runId);
					break; 
				case 3: 
					E->send(latency,event);
					printf("Manager sending work to Worker 3 at iteration %d\n\n", event -> runId);
					break;   
				case 2:
					W->send(latency,event);
					printf("Manager sending work to Worker 2 at iteration %d\n\n", event -> runId);
					break;  
				case 1:
					S->send(latency,event);
					printf("Manager sending work to Worker 1 at iteration %d\n\n", event -> runId);
					break; 
				default: 
					break; 
			}
		}  
	} 
}

bool sst_Portuno::clockTic( Cycle_t ) 
{
	SST::Event *ev = NULL;
	switch(my_id) {
		case 0: 
		{
			std::string outputFile = outputPath + "/latest.tsv";

			if(doneCount == 4) {
				struct timeval endTV;
				gettimeofday(&endTV, NULL);
				end = endTV.tv_sec + endTV.tv_usec / 1e6;
				double elapsed = end - start;
				std::cout << "Total simulation time: " << elapsed << "sec."; 

				primaryComponentOKToEndSim();
	    			return true;
			}
			while((ev = N->recv())){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);

				if (event -> waitFlag == 0) {
					if (event -> runId < count_to) {
						printf("Manager received acknowledgement at iteration %d from Worker 4\n", event->runId);
						std::string paramFile = outputPath + "/p_" +  std::to_string((event->runId * 4 + 4)) + ".txt";
						event->runId++;
						event -> populatePayload(paramFile);
						N->send(latency, event);
						printf("Manager sending work to Worker 4 at iteration %d\n\n", event -> runId);
					} else {
						writePayloadToFile(event->payload, outputFile);
						printf("Manager received consolidated output at iteration %d from Worker 4\n", event->runId);
						doneCount++;
					}
				} else {
					printf("Manager is waiting for acknowledgement at iteration %d from Worker 4\n", event->runId);
				}
			}
			while((ev = E->recv())){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);

				if (event -> waitFlag == 0) {
					if (event -> runId < count_to) {
						printf("Manager received acknowledgement at iteration %d from Worker 3\n", event->runId);
						std::string paramFile = outputPath + "/p_" +  std::to_string((event->runId * 4 + 3)) + ".txt";
						event->runId++;
						event -> populatePayload(paramFile);
						E->send(latency, event);
						printf("Manager sending work to Worker 3 at iteration %d\n\n", event -> runId);
					} else {
						writePayloadToFile(event->payload, outputFile);
						printf("Manager received consolidated output at iteration %d from Worker 3\n", event->runId);
						doneCount++;
					}
				} else {
					printf("Manager is waiting for acknowledgement at iteration %d from Worker 3\n", event->runId);
				}
			}
			while((ev = W->recv())){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);

				if (event -> waitFlag == 0) {
					if (event -> runId < count_to) {
						printf("Manager received acknowledgement at iteration %d from Worker 2\n", event->runId);
						std::string paramFile = outputPath + "/p_" +  std::to_string((event->runId * 4 + 2)) + ".txt";
						event->runId++;
						event -> populatePayload(paramFile);
						W->send(latency, event);
						printf("Manager sending work to Worker 2 at iteration %d\n\n", event -> runId);
					} else {
						writePayloadToFile(event->payload, outputFile);
						printf("Manager received consolidated output at iteration %d from Worker 2\n", event->runId);
						doneCount++;
					}
				} else {
					printf("Manager is waiting for acknowledgement at iteration %d from Worker 2\n", event->runId);
				}
			}
			while((ev = S->recv())){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);

				if (event -> waitFlag == 0) {
					if (event -> runId < count_to) {
						printf("Manager received acknowledgement at iteration %d from Worker 1\n", event->runId);
						std::string paramFile = outputPath + "/p_" +  std::to_string((event->runId * 4 + 1)) + ".txt";
						event->runId++;
						event -> populatePayload(paramFile);
						S->send(latency, event);
						printf("Manager sending work to Worker 1 at iteration %d\n\n", event -> runId);
					} else {
						writePayloadToFile(event->payload, outputFile);
						printf("Manager received consolidated output at iteration %d from Worker 1\n", event->runId);
						doneCount++;
					}
				} else {
					printf("Manager is waiting for acknowledgement at iteration %d from Worker 1\n", event->runId);
				}
			}
			break;
		}
		case 1:
		{
			while((ev = N->recv()) && waitFlag == 0){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);
				printf("Worker 1 received work at iteration %d\n", event->runId);
				printf("Working on payload\n");

				std::string paramFile = outputPath + "/param_1_" + std::to_string(event->runId) + ".txt";
				writePayloadToFile(event->payload, paramFile);

				std::string workerInterfaceCommand = "java -cp " + binaryPath + " base.WorkerInterface 1 " + std::to_string(event->runId) + " " + outputPath + " &";
				const char *cmd = workerInterfaceCommand.c_str();
				int ret = system(cmd);
				
				runId = event->runId;
				waitFlag = 1;
				event->waitFlag=1;

				N->send(latency, event);
				printf("Worker 1 requested manager to wait at iteration %d\n\n", event->runId);
			}
			
			std::string outputFile = outputPath + "/output_1_" + std::to_string(runId) + ".txt";
			
			if (waitFlag && fileExists(outputFile)) {
				waitFlag = 0;
				PortunoEvent *event = new PortunoEvent();
				event -> runId = runId;
				event -> waitFlag = 0;
				if (runId == count_to) {
					printf("\nWorker 1 sent consolidated output to manager at iteration %d\n\n", runId);
					event->populateOutputPayload(outputPath, 1, count_to);
					N->send(latency, event);
					return true;
				} else {
					printf("\nWorker 1 sent acknowledgement to manager at iteration %d\n\n", runId);
					N->send(latency, event);
				}
			}
			break;
		}
		case 2:
		{
			while((ev = E->recv()) && waitFlag == 0){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);
				printf("Worker 2 received work at iteration %d\n", event->runId);
				printf("Working on payload\n");

				std::string paramFile = outputPath + "/param_2_" + std::to_string(event->runId) + ".txt";
				writePayloadToFile(event->payload, paramFile);

				std::string workerInterfaceCommand = "java -cp " + binaryPath + " base.WorkerInterface 2 " + std::to_string(event->runId) + " " + outputPath + " &";
				const char *cmd = workerInterfaceCommand.c_str();
				int ret = system(cmd);
				
				runId = event->runId;
				waitFlag = 1;
				event->waitFlag=1;

				E->send(latency, event);
				printf("Worker 2 requested manager to wait at iteration %d\n\n", event->runId);
			}
			
			std::string outputFile = outputPath + "/output_2_" + std::to_string(runId) + ".txt";
			
			if (waitFlag && fileExists(outputFile)) {
				waitFlag = 0;
				PortunoEvent *event = new PortunoEvent();
				event -> runId = runId;
				event -> waitFlag = 0;
				if (runId == count_to) {
					printf("\nWorker 2 sent consolidated output to manager at iteration %d\n\n", runId);
					event->populateOutputPayload(outputPath, 2, count_to);
					E->send(latency, event);
					return true;
				} else {
					printf("\nWorker 2 sent acknowledgement to manager at iteration %d\n\n", runId);
					E->send(latency, event);
				}
			}
			break;
		}
		case 3:
		{
			while((ev = W->recv()) && waitFlag == 0){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);
				printf("Worker 3 received work at iteration %d\n", event->runId);
				printf("Working on payload\n");

				std::string paramFile = outputPath + "/param_3_" + std::to_string(event->runId) + ".txt";
				writePayloadToFile(event->payload, paramFile);

				std::string workerInterfaceCommand = "java -cp " + binaryPath + " base.WorkerInterface 3 " + std::to_string(event->runId) + " " + outputPath + " &";
				const char *cmd = workerInterfaceCommand.c_str();
				int ret = system(cmd);
				
				runId = event->runId;
				waitFlag = 1;
				event->waitFlag=1;

				W->send(latency, event);
				printf("Worker 3 requested manager to wait at iteration %d\n\n", event->runId);
			}
			
			std::string outputFile = outputPath + "/output_3_" + std::to_string(runId) + ".txt";
			
			if (waitFlag && fileExists(outputFile)) {
				waitFlag = 0;
				PortunoEvent *event = new PortunoEvent();
				event -> runId = runId;
				event -> waitFlag = 0;
				if (runId == count_to) {
					printf("\nWorker 3 sent consolidated output to manager at iteration %d\n\n", runId);
					event->populateOutputPayload(outputPath, 3, count_to);
					W->send(latency, event);
					return true;
				} else {
					printf("\nWorker 3 sent acknowledgement to manager at iteration %d\n\n", runId);
					W->send(latency, event);
				}
			}
			break;
		}
		case 4:
		{
			while((ev = S->recv()) && waitFlag == 0){
				PortunoEvent *event = dynamic_cast<PortunoEvent*>(ev);
				printf("Worker 4 received work at iteration %d\n", event->runId);
				printf("Working on payload\n");

				std::string paramFile = outputPath + "/param_4_" + std::to_string(event->runId) + ".txt";
				writePayloadToFile(event->payload, paramFile);

				std::string workerInterfaceCommand = "java -cp " + binaryPath + " base.WorkerInterface 4 " + std::to_string(event->runId) + " " + outputPath + " &";
				const char *cmd = workerInterfaceCommand.c_str();
				int ret = system(cmd);
			
				runId = event->runId;	
				waitFlag = 1;
				event->waitFlag=1;

				S->send(latency, event);
				printf("Worker 4 requested manager to wait at iteration %d\n\n", event->runId);
			}
			
			std::string outputFile = outputPath + "/output_4_" + std::to_string(runId) + ".txt";
			
			if (waitFlag && fileExists(outputFile)) {
				waitFlag = 0;
				PortunoEvent *event = new PortunoEvent();
				event -> runId = runId;
				event -> waitFlag = 0;
				if (runId == count_to) {
					printf("\nWorker 4 sent consolidated output to manager at iteration %d\n\n", runId);
					event->populateOutputPayload(outputPath, 4, count_to);
					S->send(latency, event);
					return true;
				} else {
					printf("\nWorker 4 sent acknowledgement to manager at iteration %d\n\n", runId);
					S->send(latency, event);
				}
			}
			break;
		}
		default:
			break;
		
	}
	return false;
}

bool sst_Portuno::fileExists(std::string fileName) {
	struct stat buffer;   
	return (stat (fileName.c_str(), &buffer) == 0);
}

void sst_Portuno::writePayloadToFile(std::vector<char> payload, std::string fileName) {
	std::ofstream textout(fileName.c_str(), std::fstream::out | std::fstream::app | std::fstream::binary);
	textout.write((const char*)&payload[0], payload.size());

	textout.close();
}

// Element Libarary / Serialization stuff
    
BOOST_CLASS_EXPORT(SST::Portuno::PortunoEvent)
BOOST_CLASS_EXPORT(SST::Portuno::sst_Portuno)

static Component* create_sst_Portuno(SST::ComponentId_t id, SST::Params& params) 
{
    return new sst_Portuno(id, params);
}

static const ElementInfoParam sst_Portuno_params[] = {
    { "id", "Node id" },
    { "count_to", "Number of iterations of the test" },
    { "latency", "Send latency for event" },
    { "driver", "Name of driver class" },
    { "configPath", "Path to json config" },
    { "binaryPath", "Path to java binary" },
    { "outputPath", "Path to store output" },
    { NULL, NULL}
};

static const char*  sst_Portuno_port_events[] = { "Portuno. sst_PortunoEvent", NULL };

static const ElementInfoPort  sst_Portuno_ports[] = {
    {"Nlink", "Link to the  sst_Portuno to the North",  sst_Portuno_port_events},
    {"Slink", "Link to the  sst_Portuno to the South",  sst_Portuno_port_events},
    {"Elink", "Link to the  sst_Portuno to the East",   sst_Portuno_port_events},
    {"Wlink", "Link to the  sst_Portuno to the West",   sst_Portuno_port_events},
    {NULL, NULL, NULL}
};

static const ElementInfoComponent components[] = {
    { "sst_Portuno",                                 	 // Name
      "Portuno Simulator",                               // Description
      NULL,                                              // PrintHelp
      create_sst_Portuno,                            	 // Allocator
      sst_Portuno_params,                            	 // Parameters
      sst_Portuno_ports,                             	 // Ports
      COMPONENT_CATEGORY_PROCESSOR,                      // Category
      NULL                                               // Statistics
    },
    { NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL}
};

extern "C" {
    ElementLibraryInfo sst_Portuno_eli = {
        "sst_Portuno",                          // Name
        "Portuno Simulator",  			// Description
        components,                         	// Components
        NULL,                                   // Events 
        NULL,                                   // Introspectors 
        NULL,                                   // Modules 
        NULL,                                   // Subcomponents 
        NULL,                                   // Partitioners
        NULL,                                   // Python Module Generator
        NULL                                    // Generators
    };
}
