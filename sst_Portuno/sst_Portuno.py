import sst
import json
import os

current_path = os.path.dirname(__file__)
src_path = os.path.join(current_path, '..', '..', '..', '..', 'banaanbill-acsimmer-0938368823ca', 'AcSimmer', 'src')
src_path = os.path.realpath(src_path)

config_path = os.path.realpath('configuration.json')

with open(config_path) as data_file:
  configuration = json.load(data_file)

num_iterations = configuration['num_iterations']
driver         = configuration['driver']
output         = configuration['output']

sst.setProgramOption("timebase", "60 s")

manager = sst.Component("manager", "sst_Portuno.sst_Portuno")
manager.addParams({
      "id" : """0""",
      "count_to" : str(num_iterations/4),
      "latency" : """10000""",
      "driver" : driver,
      "configPath": config_path,
      "binaryPath" : src_path,
      "outputPath" : output
})
manager.setRank(0);

worker1 = sst.Component("worker1", "sst_Portuno.sst_Portuno")
worker1.addParams({
      "id" : """1""",
      "count_to" : str(num_iterations/4),
      "latency" : """10000""",
      "driver" : driver,
      "configPath": config_path,
      "binaryPath" : src_path,
      "outputPath" : output
})
worker1.setRank(1);

worker2 = sst.Component("worker2", "sst_Portuno.sst_Portuno")
worker2.addParams({
      "id" : """2""",
      "count_to" : str(num_iterations/4),
      "latency" : """10000""",
      "driver" : driver,
      "configPath": config_path,
      "binaryPath" : src_path,
      "outputPath" : output
})
worker2.setRank(2);

worker3 = sst.Component("worker3", "sst_Portuno.sst_Portuno")
worker3.addParams({
      "id" : """3""",
      "count_to" : str(num_iterations/4),
      "latency" : """10000""",
      "driver" : driver,
      "configPath": config_path,
      "binaryPath" : src_path,
      "outputPath" : output
})
worker3.setRank(3);

worker4 = sst.Component("worker4", "sst_Portuno.sst_Portuno")
worker4.addParams({
      "id" : """4""",
      "count_to" : str(num_iterations/4),
      "latency" : """10000""",
      "driver" : driver,
      "configPath": config_path,
      "binaryPath" : src_path,
      "outputPath" : output
})
worker4.setRank(4);

# Define the simulation links
N = sst.Link("N")
N.connect( (manager, "Nlink", "25s"), (worker4, "Slink", "25s") )

E = sst.Link("E")
E.connect( (manager, "Elink", "25s"), (worker3, "Wlink", "25s") )

W = sst.Link("W")
W.connect( (manager, "Wlink", "25s"), (worker2, "Elink", "25s") )

S = sst.Link("S")
S.connect( (manager, "Slink", "25s"), (worker1, "Nlink", "25s") )


